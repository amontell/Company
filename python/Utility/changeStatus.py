#!/usr/bin/env python3

import os
import argparse
import json
import re
import copy

def parser():
    parser = argparse.ArgumentParser(description = "Script to change status of company worker", formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("--status", type=str)
    parser.add_argument("--changes", nargs='+')

    return parser.parse_args()

def main():
    args = parser()

    with open(args.status, "r") as companyJson:
        companyStatus = json.load(companyJson)
        newCompanyStatus = copy.deepcopy(companyStatus)
        plannedChanges = {re.compile(pattern): status for pattern, status in [change.split(":") for change in args.changes]}

        for worker in companyStatus:
            for pattern, status in plannedChanges.items():
                if re.search(pattern, worker):
                    print("Change worker status '{}': '{}' -> '{}'".format(worker, companyStatus[worker]["status"], status))
                    newCompanyStatus[worker]["status"] = status

    with open(args.status, "w") as companyJson:
        json.dump(newCompanyStatus, companyJson, indent=4)

if __name__ == "__main__":
    main()
